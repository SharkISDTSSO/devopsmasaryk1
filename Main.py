import tkinter as tk
import time
import re


from PIL import ImageTk

Tstat = "True"
Fstat = "False"

with open('Dpe_Post.txt') as my_file:
    testsite_array = my_file.readlines()

i = 1
for i in range(15):
    testsite_array[i]= re.sub("[^a-zA-Z0-9,-.@]+", "", testsite_array[i], flags=re.IGNORECASE)

    i = i + 1

    if i == 15:
        break

animation = tk.Tk()

canvas = tk.Canvas(animation, width=800, height=600)
canvas.pack()
#canvas.create_polygon(10, 10, 10, 60, 50, 35)


# load the .gif image file
gif1 = ImageTk.PhotoImage(file='./servers/servers.png')


canvas.create_image(50, 10, image=gif1, anchor=tk.NW)

canvas.create_line(320, 393, 360, 393, arrow=tk.LAST,
fill="green", width=3)


#First check FMF01
for x in range(0,10):
    canvas.move(2, 10, 0)
    animation.update()
    time.sleep(0.05)
    if x == 10:
        break

canvas.create_line(448, 460, 448, 420, arrow=tk.LAST,
fill="green", width=3)
time.sleep(2)


for x in range(0,10):
    canvas.move(3, 0, -10)
    animation.update()
    time.sleep(0.05)
    if x == 7:

        break

if testsite_array[1] == Tstat:
    print("FMF01 Transfer check ok")
    good = ImageTk.PhotoImage(file='./servers/good.png')
    canvas.create_image(401, 179, image=good, anchor=tk.NW)
    animation.update()
    time.sleep(2)
else:
    print("FMF01 Transfer check not ok")
    good = ImageTk.PhotoImage(file='./servers/bad.PNG')
    canvas.create_image(401, 179, image=good, anchor=tk.NW)
    animation.update()
    time.sleep(2)


